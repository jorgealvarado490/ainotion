import {Configuration, OpenAIApi} from 'openai-edge';

const config = new Configuration({
    apiKey: process.env.OPEN_AI_API_KEY!
})

const openai = new OpenAIApi(config);

export async function generateImagePrompt(noteName: string) {
    try {
        const response = await openai.createChatCompletion({
            model: 'gpt-3.5-turbo',
            messages: [
                {
                    role: 'system',
                    content: 'You are a helpful AI embedded in a notion text editor app that is used to autocomplete sentences. The traits of AI include expert knowledge, helpfulness, cleverness, and articulateness. AI is a well-behaved and well-mannered individual. AI is always friendly, kind, and inspiring, and he is eager to provide vivid and thoughtful responses to the user.'
                },
                {
                    role: 'user',
                    content:  `
        I am writing a piece of text in a notion text editor app.
        Help me complete my train of thought here: ##${noteName}##
        keep the tone of the text consistent with the rest of the text.
        keep the response short and sweet.
        `
                }
            ],
        });

        const data = await  response.json();
        console.log({data});
        // const image_description = data.choices[0].message.content;
        const image_description = "A nice place to live in Europe with some forest around."
        return image_description as string;
    } catch (e) {
        console.log(e);
        throw e;
    }
}

export async function generateImage(imageDescription: string) {
    try{
        const response = await openai.createImage({
            prompt: imageDescription,
            n:1,
            size: '256x256',
        });
        const data = await response.json();
        //const imageUrl = data.data[0].url;
        // returning a placeholder image because the quota from my panel open AI in insufficient
        return "https://lh3.googleusercontent.com/fife/AK0iWDxjfDix7XfBi5n1bRKR2pa85Umc10P4Bcv0r7KLohlBytt-atRdR_QpwUJtnv1zAvqXI0G1VQfEEQxkkzsI2Zcwe02U7SH0n4mIjkvWvfq4RFTD3Vef_LcACw7t2U7obtNldzlDUB5388p8UwFHIPpWJQ3B3Dg6STjpV9QMP067FBooJuzdRLHX4xVuyThxQGBPicd945q1_O1f8ofwd70JpkrxVW_EZVkDobXQn5k1cGQ8c-nliwrhIbLsdL6N8mEjzFNlj987tJmCfZ8zdKbmnv-FpKDAJaxrnGOB05_6JdZN-qd3CUsb7s3wJh9frkbF-W9f2HGxAa_v5kkQ0VPc4s9sGNH04drMqrKY-hpXTFxZ8G8kntq_1-0r4gmLyJgtIR2FykccU9WrZdsdiSsejlbzpa7QyVmToK_Cc-kM_63xFE9mEBuoyooucsN12Q4WRrZ8wvBKghm8MkTnlnTEi_9l_kD-nutJQj7uNsH5fkqJhQRp05wNv0cYhb932jrdzWq3sWF-fB4tR3_mddX88S80JUlFdGPi0-OgivPp7CC8xYpJrwR32zDdiV92czYfn4O-XIdVhZLSSHHg3EskHRRCaiSS-525gI3EZAxrVOVeQzgrRkZqnK1qzwq2IwVNAggIPMNVFDmqJnQ_NlynzJD4kNOUtmZ53UdIVPWIJ4ta0a4PPq6RXh2pOpQuNEEVebrgEGbWrCFSoglV9j_Xp0YVUiTXX11k6Nk2WOBXbVtwZDECRwb6mmXBsA8K7k4moKfcfsD06yZ1kTtGtpo95ECIo9pTOwCaFIO3Yu0Usy0zw660I5n4cnCs47X-4XmnEStPtu66cCN_t-m48pade8UIzHIKIhA3OdngGHrQMwXLDiQXii_MIpKY5RK5mAfmqdRNRrg1zrafeqArbPoFr2gQHkfntMqdpMEmxOKdETiS4TLBbEdBMnYaV-AXqz1A5lmokBHjMmmB7iv34VEoH8zjQgO0hwB_O0VJdtjo0eaoTmxf3KSrCF8N_aUoEBrxPULugqjq=w1920-h983";
    }catch (e) {
        console.error(e);
        throw e;
    }
}
