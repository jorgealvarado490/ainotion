// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getDownloadURL, getStorage, ref, uploadBytes} from "@firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: "ainotion-85948.firebaseapp.com",
    projectId: "ainotion-85948",
    storageBucket: "ainotion-85948.appspot.com",
    messagingSenderId: "932158647273",
    appId: "1:932158647273:web:d591848c969003501cf978"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const storage = getStorage(app);


export async function uploadFileToFirebase(imageUrl: string, fileName: string){
    try{
        const response = await fetch(imageUrl);
        const buffer = await response.arrayBuffer();
        const firebaseFileName = fileName.replace(' ', '') + Date.now() + '.jpeg';
        const storageRef = ref(storage, firebaseFileName);
        await uploadBytes(storageRef, buffer, {
            contentType: 'image/jpeg'
        });
        return await getDownloadURL(storageRef);
    }catch (err) {
        console.error(err);
        throw err;
    }
}
