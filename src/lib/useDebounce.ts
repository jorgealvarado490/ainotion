import {useEffect, useState} from 'react';

const useDebounce = (value: string, debounceTime: number) => {
    const [debounceValue, setDebounceValue] = useState(value);

    useEffect(() => {
        const handler = setTimeout(() => {
            setDebounceValue(value);
        }, debounceTime);

        return () => {
            clearTimeout(handler);
        }
    }, [value, debounceTime]);

    return debounceValue;
}

export default useDebounce;
