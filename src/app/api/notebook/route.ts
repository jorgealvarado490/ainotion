// /api/notebook

import {auth} from "@clerk/nextjs";
import {NextResponse} from "next/server";
import {generateImage, generateImagePrompt} from "@/lib/openai";
import {db} from "@/lib/db";
import {$notes} from "@/lib/db/schema";

export const runtime = "edge";
export async function POST(req: Request) {
    const {userId} = auth();
    if (!userId) {
        // @ts-ignore
        return new NextResponse('unauthorised', {status: 401});
    }

    const body = await req.json();
    const {name} = body;
    const imageDescription = await generateImagePrompt(name);
    if(!imageDescription) {
        // @ts-ignore
        return new NextResponse('failed to generate image description', {status: 500});
    }
    const imageUrl = await generateImage(imageDescription);
    if(!imageUrl){
        // @ts-ignore
        return new NextResponse('failed to generate image', {status: 500});
    }

    const notes_ids = await db.insert($notes).values({
        name,
        userId,
        imageUrl
    }).returning({
        insertedId: $notes.id
    });

    return NextResponse.json({
        noteId: notes_ids[0].insertedId
    });
}
