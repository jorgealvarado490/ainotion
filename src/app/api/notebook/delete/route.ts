import {NextRequest, NextResponse} from "next/server";
import {db} from "@/lib/db";
import {$notes} from "@/lib/db/schema";
import {eq} from "drizzle-orm";

export async function POST(req: NextRequest) {
    try {
        const {noteId} = await req.json();
        if (!noteId) {
            return new NextResponse('No NoteId provided', {status: 400});
        }
        await db.delete($notes).where(
            eq($notes.id, parseInt(noteId))
        );

        return NextResponse.json({
            success: true
        }, {status: 200})

    } catch (err) {
        return NextResponse.json({
            success: false,
        }, {status: 500})
    }

}
