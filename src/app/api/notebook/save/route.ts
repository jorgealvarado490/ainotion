import {NextRequest, NextResponse} from "next/server";
import {db} from "@/lib/db";
import {$notes} from "@/lib/db/schema";
import {eq} from "drizzle-orm";

export async function POST(req: NextRequest) {
    try {
        const body = await req.json();
        const {noteId, editorState} = body;
        if (!noteId || !editorState) {
            return new NextResponse('Missing editorState or NoteId', {status: 400});
        }

        const noteIntId = parseInt(noteId);
        const notes = await db.select().from($notes).where(
            eq($notes.id, noteIntId)
        );

        if (notes.length !== 1) {
            return new NextResponse('Failed to update the note', {status: 500});
        }

        const noteToUpdate = notes[0];

        if (noteToUpdate.editorState !== editorState) {
            await db.update($notes).set({
                editorState
            }).where(
                eq($notes.id, noteIntId)
            )
        }

        return NextResponse.json({
            success: true
        }, {
            status: 200
        })

    } catch (err) {
        console.error(err);
        return NextResponse.json({
            success: false
        }, {status: 500})
    }
}
