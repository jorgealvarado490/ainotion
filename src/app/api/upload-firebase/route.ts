import {NextRequest, NextResponse} from "next/server";
import {db} from "@/lib/db";
import {$notes} from "@/lib/db/schema";
import {eq} from "drizzle-orm";
import {uploadFileToFirebase} from "@/lib/firebase";

export async function POST(req: NextRequest) {
    try {
        const {noteId} = await req.json();
        if(!noteId){
            return new NextResponse('No noteId provided', {status: 400});
        }
        const notes = await db.select().from($notes).where(
            eq($notes.id , parseInt((noteId)))
        );

        if(notes.length !== 1){
            return new NextResponse('No note founded with that id', {status: 404});
        }

        const note = notes[0];

        if(!note.imageUrl){
            return new NextResponse('No image was provided', {status: 400});
        }

        const firebaseUrl = await uploadFileToFirebase(note.imageUrl, note.name);
        await db.update($notes).set({
            imageUrl: firebaseUrl
        }).where(
            eq($notes.id , parseInt((noteId)))
        );

        return NextResponse.json({
            success: true
        }, {status: 200})

    }catch (err) {
        return NextResponse.json({
            success: false
        }, {status: 500})
    }
}
