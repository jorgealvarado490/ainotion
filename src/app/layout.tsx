import './globals.css'
import type {Metadata} from 'next'
import {Inter} from 'next/font/google'
import {ClerkProvider} from "@clerk/nextjs";
import Providers from "@/components/Providers";
import React from "react";

const inter = Inter({subsets: ['latin']})

export const metadata: Metadata = {
    title: 'AiNotion',
    description: 'App like Notion using AI, ShadCDN, ClerkAuth',
}

export default function RootLayout({
                                       children,
                                   }: {
    children: React.ReactNode
}) {
    return (
        <ClerkProvider>
            <html lang="en">
            <Providers>
                <body className={inter.className}>{children}</body>
            </Providers>
            </html>
        </ClerkProvider>
    )
}
