import {Button} from "@/components/ui/button";
import TypeWriterTitle from "@/components/TypeWriterTitle";
import Link from "next/link";
import {ArrowRight} from "lucide-react";

export default function Home() {
    return (
        <div className="bg-gradient-to-r grainy min-h-screen from-rose-100 to-teal-100 h-full">
            <div className="flex flex-col h-full items-center justify-center">
                <h1 className="font-semibold text-7xl text-center select-none">AI <span
                    className="text-green-600 font-bold">note taking</span> assistant</h1>
                <div className="mt-4"></div>
                <h2 className="font-semibold text-3xl text-center text-slate-700 select-none">
                    <TypeWriterTitle/>
                </h2>
                <div className="mt-8"></div>
                <Link href="/dashboard">
                    <Button className="bg-green-600">Get Started
                        <ArrowRight className="ml-2 w-5 h-5" strokeWidth={2}/>
                    </Button>
                </Link>
            </div>
        </div>
    )
}
