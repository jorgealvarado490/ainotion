"use client";

import {
    Dialog,
    DialogContent,
    DialogDescription,
    DialogHeader,
    DialogTitle,
    DialogTrigger
} from "@/components/ui/dialog";
import {Loader2, Plus} from "lucide-react";
import {Input} from "@/components/ui/input";
import {Button} from "@/components/ui/button";
import {FormEvent, useState} from "react";
import {useMutation} from "@tanstack/react-query";
import axios from "axios";
import {useRouter} from "next/navigation";

const CreateNoteDialog = () => {

    const router = useRouter();

    const [inputName, setInputName] = useState<string>('');

    const updateImageToFirebase = useMutation({
        mutationFn: async (noteId: string) => {
            const response = await axios.post('/api/upload-firebase', {
                noteId: noteId
            })
            return response.data;
        }
    })

    const createNotebook = useMutation({
        mutationFn: async () => {
            const response = await axios.post('api/notebook', {
                name: inputName
            });
            return response.data;
        }
    })

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (!inputName) {
            window.alert('Please enter a name for your notebook');
            return;
        }

        createNotebook.mutate(undefined, {
            onSuccess: ({noteId}) => {
                updateImageToFirebase.mutate(noteId);
                router.push(`/notebook/${noteId}`);
            },
            onError: (error) => {
                console.error(error);
                window.alert('Failed to create new notebook');
            }
        })
    }

    return (
        <Dialog>
            <DialogTrigger>
                <div
                    className="border-dashed border-2 border-green-600 h-full rounded-lg flex items-center justify-center sm:flex-col hover:shadow-xl transition hover:-translate-y-1 flex-row p-4">
                    <Plus className="w-6 h-6 text-green-600" strokeWidth={3}/>
                    <h2 className="font-semibold text-green-600 sm:mt-2">New Note Book</h2>
                </div>
            </DialogTrigger>
            <DialogContent>
                <DialogHeader>
                    <DialogTitle>
                        New Note
                    </DialogTitle>
                    <DialogDescription>
                        You can create a new note by clicking the button below.
                    </DialogDescription>
                </DialogHeader>
                <form onSubmit={handleSubmit}>
                    <Input placeholder="Name..." value={inputName}
                           onChange={event => setInputName(event.target.value.toLocaleUpperCase())}/>
                    <div className="h-4"></div>
                    <div className="flex items-center gap-2">
                        <Button type="reset" variant={"secondary"}>Cancel</Button>
                        <Button type="submit" className="bg-green-600" disabled={createNotebook.isPending || updateImageToFirebase.isPending}>
                            {(createNotebook.isPending || updateImageToFirebase.isPending) && (
                                <Loader2 className="w-4 h-4 mr-2 animate-spin"/>
                            )}
                            Create
                        </Button>
                    </div>
                </form>
            </DialogContent>
        </Dialog>
    );
};

export default CreateNoteDialog;
