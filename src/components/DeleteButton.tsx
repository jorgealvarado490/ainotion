'use client';

import {Button} from "@/components/ui/button";
import {Loader2, Trash} from "lucide-react";
import {useMutation} from "@tanstack/react-query";
import axios from "axios";
import {useRouter} from "next/navigation";

type Props = {
    noteId: number
}
const DeleteButton = ({noteId}: Props) => {

    const router = useRouter();

    const deleteNote = useMutation({
        mutationFn: async () => {
            const response = await axios.post('/api/notebook/delete', {
                noteId
            });
            return response.data;
        }
    })

    const handleDelete = () => {
        const confirm = window.confirm('Are you sure you want to delete this note?');
        if (!confirm) return;

        deleteNote.mutate(undefined, {
            onSuccess: () => {
                console.log('Note deleted');
                router.push('/dashboard');
            },
            onError: (err) => {
                console.error(err);
            }
        })

    }

    return (
        <Button className="ml-auto" type={'button'} variant={'destructive'} size={'sm'} onClick={handleDelete}
                disabled={deleteNote.isPending}>
            {deleteNote.isPending ?
                (<Loader2 className="w-4 h-4 mr-2 animate-spin"/>) :
                <Trash/>
            }
        </Button>
    );
};

export default DeleteButton;
