'use client';

import {EditorContent, useEditor} from '@tiptap/react'
import StarterKit from '@tiptap/starter-kit'
import {useEffect, useState} from "react";
import TipTapMenuBar from "@/components/TipTapMenuBar";
import {Button} from "@/components/ui/button";
import useDebounce from "@/lib/useDebounce";
import {useMutation} from "@tanstack/react-query";
import axios from "axios";
import {NoteType} from "@/lib/db/schema";

type Props = {
    note: NoteType
}
const TipTapEditor = ({note: {id, editorState: editorStateNote, name}}: Props) => {

    const [editorState, setEditorState] = useState(editorStateNote || `<h1>${name}</h1>`);

    const saveNote = useMutation({
        mutationFn: async () => {
            const response = await axios.post('/api/notebook/save', {
                noteId: id,
                editorState
            })
            return response.data;
        }
    })

    const editor = useEditor({
        autofocus: true,
        extensions: [
            StarterKit,
        ],
        content: editorState,
        onUpdate: ({editor}) => {
            setEditorState(editor.getHTML());
        }
    })

    const debounceEditorState = useDebounce(editorState, 900);

    useEffect(() => {
        if (debounceEditorState === '') return;
        saveNote.mutate(undefined, {
            onSuccess: ((data) => {
                console.log('Sucess update', data)
            }),
            onError: ((err) => {
                console.error(err);
            })
        })
    }, [debounceEditorState]);

    return (
        <>
            <div className="flex justify-between">
                {editor && <TipTapMenuBar editor={editor}/>}
                <Button disabled={true} variant={'outline'}>
                    {saveNote.isPending ? 'Saving...' : 'Saved'}
                </Button>
            </div>

            <div className="prose prose-sm w-full mt-4">
                <EditorContent editor={editor}/>
            </div>
            <div className="h-4"></div>
            <span className="text-sm">
                Tip: Press
                {' '}
                <kbd
                    className="px-2 py-1.5 text-xs font-semibold text-gray-800 bg-gray-100 border border-gray-200 rounded-lg">Shift + A</kbd>
                {' '}for AI completion.
            </span>
        </>
    )
};


export default TipTapEditor;
