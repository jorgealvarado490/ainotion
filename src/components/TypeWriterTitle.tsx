'use client';

import Typewriter from 'typewriter-effect';

type Props = {}
const TypeWriterTitle = (props:Props) => {
    return (
        <Typewriter options={{
            loop: true,
        }} onInit={(typewriter) => {
            typewriter.typeString("🚀 Enhance your productivity")
            .pauseFor(1000)
            .deleteAll()
            .typeString("🧰 AI-Powered Insights")
                .start();
        }}/>
    );
};

export default TypeWriterTitle;
